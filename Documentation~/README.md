# GNU Emacs for Unity
This is a Unity Package for GNU Emacs editor integration.  It uses the VSCode package as the base for functionality, and is currently developed to work on Windows (with Mac and Linux support planned).

**This is still very much a work in progress**

## Installation
Follow the instructions for instally a custom Unity package.  The following are probably the easiest methods:
- [Installing a package from a local folder](https://docs.unity3d.com/Manual/upm-ui-local.html "Installing a package from a local folder")
- [Installing from a Git URL](https://docs.unity3d.com/Manual/upm-ui-giturl.html "Installing from a Git URL")

This package is designed to work with emacsclientw.exe on Windows in order to reuse the existing Emacs session. If no Emacs session is already started, a new session will be created with the first file opened.

Once the package has been installed, as long as the Emacs installation is located in the Program Files directory, GNU Emacs will appear in the External Script Editor dropdown menu located in the External Tools Preferences.
